<div id="portrait">
	<div class="container">
		<div class="row">
			<div class="container-form">
				<form class="form-inline">
				  <div class="form-group mr-md-4">
				    <label for="inlineFormCustomSelectPref" class="ml-md-4 mr-md-4">Buscar</label>
				    <input class="form-control" id="inputPassword2" placeholder="Plomero, Electricista, ..." type="password">
				  </div>
				  <button type="submit" class="btn btn-primary">luis eduardo encontrar al mejor personal</button>
				</form>
			</div>
		</div>
	</div>
	<div class="container">
		<h1 class="text-center text-light">Encuentra al personal adecuado para el trabajo que deseas realizar, en tu casa, tu oficina, tus muebles, tu auto, etc.</h1>
		<p class="text-center">
			<i class="text-light fas fa-chevron-down f-s-4-em"></i>
		</p>
	</div>
</div>
<div class="mt-4 d-flex"></div>
<div class="container">
	<div class="row">
		<div class="col-sm-12 text-center">
			<h1>EL PERSONAL MÁS DESTACADO</h1>
		</div>
		<div class="col-md-4">
		<div class="card">
		  <img class="card-img-top" src="https://loremflickr.com/100/80/plumber" alt="Card image cap">
		  <div class="card-body">
		    <h5 class="card-title">Plomeria Ávila SA de CV</h5>
		    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    <a href="#" class="btn btn-primary">Ver Más</a>
		  </div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card">
		  <img class="card-img-top" src="https://loremflickr.com/100/80/machine" alt="Card image cap">
		  <div class="card-body">
		    <h5 class="card-title">Mécanica Naranja</h5>
		    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    <a href="#" class="btn btn-primary">Go somewhere</a>
		  </div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="card">
		  <img class="card-img-top" src="https://loremflickr.com/100/80/handyman?random=3" alt="Card image cap">
		  <div class="card-body">
		    <h5 class="card-title">Card title</h5>
		    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    <a href="#" class="btn btn-primary">Go somewhere</a>
		  </div>
		</div>
	</div>
	</div>
</div>